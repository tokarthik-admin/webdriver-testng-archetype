Webdriver TestNG Archetype
==========================

This archetype generates a new Maven project that can be used as a start point to develop tests in Java with Webdriver and TestNG.
